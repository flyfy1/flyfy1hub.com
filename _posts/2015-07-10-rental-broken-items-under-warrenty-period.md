---
layout: post
title: "Rental Broken Items Under Warrenty Period"
description: "We're renting the HDB in Blk681 Race Course Road, and here're some
defact items"
category: "rental"
tags: [rental]
---

We're renting an HDB in Race Course Road. Today is the last day of the warrenty
period, and here're some defact items:

## Window Cannot close properly

![Window Cannot Close](https://dl.dropboxusercontent.com/u/9778027/rental/1.window-cannot-close.jpg)

Based on the service man of the aircon, this window cannot be properly closed is
due to the window is not in the correct track. IN the long term, this may also
harm the lock of the window.

## Plug Lever of the Sink cannot Lift up the Plug very much

When pressed down, The plug lever can only lift up the plug, even when pressed
very hard. Here is the picture:

![Plug Lever cannot lift up the
Plug](https://dl.dropboxusercontent.com/u/9778027/rental/2.sink-cannot-lift-up.jpg)

## Plug can be taken out directly

The plug is just put upon the hole, one can easily lift it up.

![Plug can be lift up
easily](https://dl.dropboxusercontent.com/u/9778027/rental/2.sink-can-takeout-directly.jpg)


## Some broken area in the side of the closet (master room)

This is the closet appear when enterring the master bedroom; those defects are
at the side of the closet. I'm planing to put a plastic tap on top of it, to
avoid further damage.

![Damage on the side of the closet](https://dl.dropboxusercontent.com/u/9778027/rental/3.broken-closet-side-view.jpg)

Here's a pic when zoomed in:

![Damage on the side of closet, zoomed](https://dl.dropboxusercontent.com/u/9778027/rental/3.broken-closet-side.jpg)

## Some broken area at the top of teh closet (master room)

Some defacts also appear at the top.

![Damage top of closet](https://dl.dropboxusercontent.com/u/9778027/rental/4.broken-closet-top.jpg)


