---
layout: post
title: "Clash of Clans -- Progresses"
description: "Recording my progresses of playing the game 'Clash of clans', constantly updated"
category: "progress-recording"
tags: ['games']
---

I started this game long time ago during exam period. At that time I played up to upgrading my base to lvl2, and found that I had to wait for a couple of hours... Then I just stopped playing and deleted the game right away (that was sure a correct choice).

#### 16th ~ 19th, Dec, 2013
Since I was in holiday, I was quite relaxed mentally -- although I have lots of work to do, I sort of distracted here and there. I remember picking up that game at the begining of this week (either 15th, or 16th); still I got impatient about the long time it took to upgrade the base. Things changed when I got no resource and I started to search for other players and 'attack' them. That was giving me a great sense of accomplishment. I was so addidicted into this game that.. I just spent hours and hours (or even days and days) using my low level troops (which means, in-efficient in farming) to look for other's village, attack them; I repeated this process until I got full with my resources. I also spent 10USD on this game to buy the gems, which enables me to buy two extra farmers.

I upgraded my town hall into level3, and further into level4 at that time. When I was upgrading it into level4, I fully upgraded my other buildings.. which left me no other place to spend my resources... That was a bad strategy.


#### 20th ~ 21st, Dec, 2013
I started to farm for resources.. One thing I noticed was that lots of players place their base outside, so that the low-level player can easily attack it, without losing much resource because the low-level troops cannot attack the well-protected resources yet. So I went looking for such kind of bases.

Another thing I've noticed was that.. goblins were very efficient when robing other players. Once I got the mortar removed(using gaints), my goblins can do basicly all the rest of thing for me. So I use the combination of: 6 gaints, 2 wall-breaker, and goblins for the reest, that was very efficient when I was lucky.. (kind of paradox right...). But usually I spent quite lots of time looking for a easy-and-rich base to attack.

Additionally, learnt from the issue of busy-waiting of late upgrading last time, I started to upgrade my base early, once I got most of the army buildings & defense building upgraded. The walls are pretty expensive to upgrade. The cost of everything in this game grow exponentially, but the walls is more obvious because upgrading walls is instant, and there're just too many of the walls.


#### 22nd, Dec, 2013
I'm waiting for my buildings to upgrade now.. Since I believe taht I'll be playing this game for long already, I spent another 25SGD to buy the buider. After that... I got full 5-builder in the game, which can boost up the progress. I now understood why this game is making so much money for SuperCell.

This my current base:
![Clash of Clan -- Base on 22nd, Dec](https://dl.dropboxusercontent.com/u/9778027/images/clash_of_clans-2013-12-22.png)

#### 23rd, Dec, 2013
This is the end of yesterday and the start of today. I spent quite some time farming for the resources.. upgraded the cratical breaks of my walls, and started to upgrade the resource-generating items. Oh BTW, I found my defense was pretty effective; and it doesn't lose much if I am under attack.
This is my base now:
![Clash of Clan -- Base on 23rd, Dec](https://dl.dropboxusercontent.com/u/9778027/images/clash_of_clans-2013-12-23.png)


#### 24th, Dec, 2013
Still farming on that day.. didn't spend much time on this therefore. The base is pretty much like the day before. I spent the extra free builders to upgrade my mines, which are taking lots of time.

Current base:
![Clash of Clan -- Base on 24nd, Dec](https://dl.dropboxusercontent.com/u/9778027/images/clash_of_clans-2013-12-24.png)


#### 25th, Dec, 2013
My town hull was upgraded into level 5. More buildings were unlocked and more to be upgraded. I bought all the possible buildings, and start upgrading process.

It's taking a long time and I spent quite some time farming today. However, I ended up founding that game to be meaningless... I just repeat this process, and I can finally get a beautiful base. But what's this for? I cannot gain anything out of my fully-upgraded base, despete the fact that I've put in nearly 40SGD into this game.

I think it's probably a pretty nice time to call a hault on this game. With the mindset of 'end with the beginning in mind', if I know for sure that the result would be ended up no use no matter how hard I tried... though that process is enjoyable, there're lots of other long-term more enjoyable things. Then why should I do that bad-result thing in the first place? it doesn't worth it.

This is my Current base. Apart from the upgrading and farming I've done, I've also bought the national flag of Singapore and China. That perfectly marks an end.
![Clash of Clan -- Base on 24nd, Dec](https://dl.dropboxusercontent.com/u/9778027/images/clash_of_clans-2013-12-25.png)

I hereby declare that this would be the end of this post(hopefully).
