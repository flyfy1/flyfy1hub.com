---
layout: page
title: "Interesting Projects"
description: ""
---
## dotFiles

On Github: <https://github.com/flyfy1/dotFiles>

##### Motivation
There're lots of [dot files](https://github.com/search?q=dotfiles) on the
internet. What's so special about this? It does the following extra things:

- auto download the list of vim plugins
- auto creating the symbolic link back to the home folder
- in the case that you've added a new dot files, you can easy move the files
  into this project folder, and create a corresponding symbolic link, with just
  one simple command.


## BeautifulCommandHandler

On Github: <https://github.com/flyfy1/BeautifulCommandHandler>

##### Motivation

It's a pattern, to show case the parsing of command line.

It's beautiful.


## uvaProblems

On Github: <https://github.com/flyfy1/uvaProblems>

##### Motivation

Nothing, but just a record of my solved UVA Problems.


## Keyboard-Mouse

On Github: <https://github.com/flyfy1/keyboard-mouse>

##### Motivation
Moving hand away from the key-board, just to perform a mouse click, is
time-consuming.

As a programmer, I want to make my hand stay to the key-board at all time, so
that I can give commands faster, from my key-board.

This is done for that purpose.


## JavaRobot4Console

On Github: <https://github.com/flyfy1/JavaRobot4Console>

##### Motivation
Yet another use of Robot. This would listen to the network message. Based on
different message format, it can do:

- Move & click mouse
- Take screenshot
