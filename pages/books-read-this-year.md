---
layout: page
title: 2016-2017年读了的书
permalink: /books-2016/
---

Listing a list of books in the past. I was originally including it within one
of the blog posts.. but then I feel this information should be separated
instead.

Books that I highly recommend has been put in bold.

## Books - 2017

- Jan
  - 以色列，一个国家的诞生 (book from WeRead, 关于以色列的诞生，一个一直处于生存
    威胁中的国家)
  - 你今天真好看 (book from WeRead)
  - 大道至简 (book from WeRead, inspiring upon mgmt)
  - 把时间当朋友 (book from WeRead, re-read, with 7 reading notes published)
    useful)
- Feb
  - *伟大的博弈* (book from WeRead, history of the WallStreet)
  - *拖延心理学* (book from WeRead, good advices on procastination)
  - Google工作整理术 (book from WeRead, too old, only tips on google are still
  - *人工智能时代* (book from WeRead, definitely good thoughts on AI)
  - 制冷少女 (book from WeRead, lame jokes)
- March
  - 绝对小孩 (book from WeRead, 给成人看的童话)
  - 没人听过兔子说的话 (book from WeRead, 纯粹的童话)
  - *野兽绅士* (book from WeRead, good advices on dating)
  - *万历十五年* (book from WeRead, a year of insignificance)
- April
  - Web前端黑客技术解密(book from WeRead, basically about CSRF and XSS, a bit on
    Click Hijacking)

## Books - 2016

- Jan
  - World Order (book from the Book Club)
  - 今天也要用心过生活 : 生活中的巧思与发现笔记01 (book from Douban Read)
  - 拆掉思维里的墙 : 原来我还可以这样活 (book from WeRead)
- Feb
  - 富爸爸——财务自由之路 (book from WeRead)
  - 皮囊 (book from Book Club)
  - MongoDB data modeling (book read at work, from Safari Books Online)
  - 切蛋糕 (book from Douban Read)
  - ReactJS the Rails Way (book read for work, from online)
  - Efficient Rails DevOps (book read for work.. very expensive.. nearly 100 SGD
    T_T)
  - 时间旅行者的妻子(book from Book Club)
- March
  - 如何高效学习 (book from WeRead, good one)
  - Promise迷你书 (book read at Work, from Online)
  - 天堂向左 深圳往右 (book from WeRead, recommend by Friend)
  - 粉红牢房效应 (book from WeRead, good one)
  - 富爸爸——21世纪的生意 (book from WeRead, just for the building of Financial
    Knowledge)
  - 新生 (book from Online. It's a really really good one. I recommend everyone
    to read)
  - 九败一胜 (book from WeRead)
- April
  - 魔鬼搭讪学 (book from WeRead, inspiring but not useful)
  - 富爸爸——辞职创业 (book from WeRead, inspiring but not very useful)
  - 案例：聪明人创业为什么容易失败 (book from WeRead. Summary: 失败的原因：精力
    涣散, 忽略市场, 股权矛盾)
  - 吾意独怜才 (book from WeRead, collection of Essays. Interesting & arrogant)
  - 淘宝产品 十年事 (book from WeRead, forgot most of it already..)
  - 我看电商 (book from WeRead, Frank & Direct & Succinct)
  - 必然 (book from WeRead, Inspiring, going to host a discussion in Book Club
- May
  - 富爸爸——提高你的财商 (book from WeRead, inspiring)
  - 富爸爸 实践篇 (book from WeRead, mostly garbage)
  - 当尼采哭泣 (book from WeRead, interesting novel about phycology)
  - 清醒思考的艺术 (book for Book Club, interesting)
  - The Complete Guide to Rails Performance (bought online, super useful)
  - 如何和老板谈加薪 (book from WeRead.. well.. didn't help much)
  - 时间移民 (book from WeRead, good novels)
- June
  - 乖 摸摸头 (book from WeRead, inspiring)
  - 阿弥陀佛么么哒 (book from WeRead, inspiring)
  - 微信帝国 (book from WeRead, short & inspiring)
  - 牧羊少年奇幻之旅 (book from WeRead, on persuing dream, inspiring)
  - 三体——地球往事
  - 三体——黑暗森林
  - 三体——死神永生 (book for BookClub. Nice Novel.. managed to complete 3 of
    them in 4 days)
  - 安静 : 内向性格的竞争力 (book from Kindle. Inspiring; helps when doing
    networking)
  - 时间的玫瑰 (book from WeRead.. basically about compound interest. Haven't
    used the tech. inside yet)
  - 如何搞电商网站 (book from WeRead.. quite old.. but still some are useful)
  - tmux Taster (book from Safari Books Online.. as one of my weekend challenge.
    Good read, learnt sth new & useful)
- July
  - 马云-我的世界永不言败 (Audio Book. Inspiring)
  - 教父 (book from BookClub. Great Novel)
  - 重力小丑 (book from WeRead.. well.. wordy)
  - 第二次机器革命 (book from BookClub.. I hosted that session.. and read this
    book twice. therefore, Inspiring)
  - 自控力 (Audio Book. Inspiring.. but the techniques are not used)
- Aug
  - eloquent ruby (Book from Safari Books Online. Nice tech book.. spent 2month
    read about it)
  - 未来边缘 (Book from WeRead. Nice novel)
  - 解忧杂货铺 (from WeRead. Nice novel. Heart-warming)
  - 竟然想通了 (from WeRead.. Pure `chick-soup`.. ideas are useful but stories
    are useless)
  - 疯传 (Book I like, summarized the basis about how sth spreaded)
- Sept
  - 硅谷钢铁侠 (Inspiring.. wanna be such influencial person)
  - 我是高频交易工程师 (book from Zhihu; when thinking about career)
  - 富爸爸 点石成金 ("Rich Dad" series.. I've probably taken too much of them)
  - 工厂女孩 (true stroy; learn to value what I currently have)
- Oct
  - 最好的告别 (for the book club, about death and getting old. Inspiring)
  - 十亿美元的教训 (for personal interest; need to read again)
  - Upgrading to Java 8 (small simple quick reference book to pickup Java8)
